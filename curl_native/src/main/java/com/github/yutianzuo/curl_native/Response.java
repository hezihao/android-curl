package com.github.yutianzuo.curl_native;

import java.io.Serializable;

/**
 * 响应实体
 */
public class Response implements Serializable {
    /**
     * 响应的数据
     */
    public String response;
}
