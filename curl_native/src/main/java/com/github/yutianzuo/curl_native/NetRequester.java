package com.github.yutianzuo.curl_native;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

/**
 * 请求封装类
 */
public enum NetRequester {
    INSTANCE;

    public void init(Context context) {
        HttpManager.INSTANCE.Uninit();
        HttpManager.INSTANCE.Init(5, context);
    }

    public void unInit() {
        HttpManager.INSTANCE.Uninit();
    }

    public static class HttpResultCallback {
        public void success(Response data) {
        }

        public void fail(int errorCode) {
        }

        public void progress(float percent) {
        }
    }

    static public class UrlBuilder {
        private RequestManager request;
        private String mPath = "";
        private String mJson;
        private final Map<String, String> headers = new HashMap<>();
        private final Map<String, String> urlParams = new HashMap<>();
        private final Map<String, String> formDataParams = new HashMap<>();

        private String mJsonName;
        private String mFormName;
        private String mFileKeyName;
        private String mFileName;
        private String mFilePath;
        private String mDownFilePath;

        public UrlBuilder with(RequestManager request) {
            this.request = request;
            return this;
        }

        public UrlBuilder addHeader(String key, String value) {
            headers.put(key, value);
            return this;
        }

        public UrlBuilder addUrlParam(String key, String value) {
            urlParams.put(key, value);
            return this;
        }

        public UrlBuilder setPath(String strPath) {
            mPath = strPath;
            return this;
        }

        public UrlBuilder addFormData(String key, String value) {
            formDataParams.put(key, value);
            return this;
        }

        public UrlBuilder setJson(String strJson) {
            this.mJson = strJson;
            return this;
        }

        public UrlBuilder setFormName(String formName) {
            mFormName = formName;
            return this;
        }

        public UrlBuilder setJsonName(String jsonName) {
            mJsonName = jsonName;
            return this;
        }

        public UrlBuilder setFileKeyName(String fileKeyName) {
            mFileKeyName = fileKeyName;
            return this;
        }

        public UrlBuilder setFileName(String fileName) {
            mFileName = fileName;
            return this;
        }

        public UrlBuilder setFilePath(String filePath) {
            mFilePath = filePath;
            return this;
        }

        public UrlBuilder setDownloadFilePath(String filePath) {
            mDownFilePath = filePath;
            return this;
        }

        public void get(final HttpResultCallback callback) {
            request.get(mPath, headers, urlParams, new HttpCallback() {
                @Override
                public void success(String response) {
                    Response bean = new Response();
                    bean.response = response;
                    callback.success(bean);
                }

                @Override
                public void progress(float percent) {
                    callback.progress(percent);
                }

                @Override
                public void fail(int errorCode) {
                    callback.fail(errorCode);
                }
            });
        }

        public void postFormData(final HttpResultCallback callbackBiz) {
            request.postForm(mPath, headers, formDataParams, new HttpCallback() {
                @Override
                public void success(String response) {
                    Response bean = new Response();
                    bean.response = response;
                    callbackBiz.success(bean);
                }

                @Override
                public void progress(float percent) {
                    callbackBiz.progress(percent);
                }

                @Override
                public void fail(int errorCode) {
                    callbackBiz.fail(errorCode);
                }
            });
        }

        public void postJson(final HttpResultCallback callbackBiz) {
            request.postJson(mPath, headers, mJson, new HttpCallback() {
                @Override
                public void success(String response) {
                    Response bean = new Response();
                    bean.response = response;
                    callbackBiz.success(bean);
                }

                @Override
                public void progress(float percent) {
                    callbackBiz.progress(percent);
                }

                @Override
                public void fail(int errorCode) {
                    callbackBiz.fail(errorCode);
                }
            });
        }

        public void putJson(final HttpResultCallback callbackBiz) {
            request.putJson(mPath, headers, mJson, new HttpCallback() {
                @Override
                public void success(String response) {
                    Response bean = new Response();
                    bean.response = response;
                    callbackBiz.success(bean);
                }

                @Override
                public void progress(float percent) {
                    callbackBiz.progress(percent);
                }

                @Override
                public void fail(int errorCode) {
                    callbackBiz.fail(errorCode);
                }
            });
        }

        public void postFile(final HttpResultCallback callbackBiz) {
            request.postFile(mPath, headers, mFormName, formDataParams, mJsonName, mJson, mFileKeyName, mFilePath,
                    mFileName, new HttpCallback() {
                        @Override
                        public void success(String response) {
                            Response bean = new Response();
                            bean.response = response;
                            callbackBiz.success(bean);
                        }

                        @Override
                        public void progress(float percent) {
                            callbackBiz.progress(percent);
                        }

                        @Override
                        public void fail(int errorCode) {
                            callbackBiz.fail(errorCode);
                        }
                    });
        }

        public void downloadFile(final HttpResultCallback callbackBiz) {
            request.downloadFile(mPath, headers, urlParams, mDownFilePath,
                    new HttpCallback() {
                        @Override
                        public void success(String response) {
                            Response bean = new Response();
                            bean.response = response;
                            callbackBiz.success(bean);
                        }

                        @Override
                        public void progress(float percent) {
                            callbackBiz.progress(percent);
                        }

                        @Override
                        public void fail(int errorCode) {
                            callbackBiz.fail(errorCode);
                        }
                    });
        }
    }
}