package com.github.yutianzuo.curl_native;

/**
 * 回调接口
 */
public interface HttpCallback {
    /**
     * 请求成功
     *
     * @param response 响应内容
     */
    void success(String response);

    /**
     * 进度更新
     *
     * @param percent 进度百分比
     */
    void progress(float percent);

    /**
     * 请求失败
     *
     * @param errorCode 错误码
     */
    void fail(int errorCode);
}
