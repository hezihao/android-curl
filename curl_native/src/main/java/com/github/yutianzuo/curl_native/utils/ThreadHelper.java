package com.github.yutianzuo.curl_native.utils;

import android.os.Looper;

public class ThreadHelper {
//    private static final ExecutorService backgroundTasksExecutor = Executors
//            .newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2, new PriorityThreadFactory());

    public static void runOnUiThread(Runnable action) {
        ThreadUtils.runOnUiThread(action);
    }

    public static boolean threadInMain() {
        return Thread.currentThread().getId() == Looper.getMainLooper().getThread().getId();
    }

//    public static Future<?> runOnBackgroundThread(Runnable action) {
//        if (action == null) {
//            return null;
//        }
//        return backgroundTasksExecutor.submit(action);
//    }
}
