package com.github.yutianzuo.myapplication;

import android.content.Context;

import com.github.yutianzuo.curl_native.HttpManager;
import com.github.yutianzuo.curl_native.RequestManager;
import com.github.yutianzuo.curl_native.utils.Misc;

public class RequestCenter {

    public static RequestManager getBizRequestManager20(Context context) {
        RequestManager request = HttpManager.INSTANCE.getRequest();
        request.setHost("https://nghttp2.org");
        request.setCertPath(Misc.getAppDir(context) + Misc.CERT_NAME);
        return request;
    }

    public static RequestManager getBizRequestManager(Context context) {
        RequestManager request = HttpManager.INSTANCE.getRequest();
        request.setHost("https://baidu.com");
        request.addBasicHeader("MyCookie", "123456789");
        request.addBasicHeader("MyCookie2", "123456789123456");
        request.addBasicUrlParams("param1", "value");
        request.addBasicUrlParams("param2", "value");
        request.setCertPath(Misc.getAppDir(context) + Misc.CERT_NAME);
        return request;
    }

    public static RequestManager getBizRequestManager2(Context context) {
        RequestManager request = HttpManager.INSTANCE.getRequest();
        request.setHost("http://example.com/");
        request.setCertPath(Misc.getAppDir(context) + Misc.CERT_NAME);
        return request;
    }

    public static RequestManager getBizRequestManager3(Context context) {
        RequestManager request = HttpManager.INSTANCE.getRequest();
        request.setHost("https://s3.meituan"
                + ".net/v1/mss_bf7e9f1c1cc54cfb819fc8ffcf965b40/boss/2.13.2/boss_assemble-2.13.2-release-guanjia"
                + ".apk");
        request.setCertPath(Misc.getAppDir(context) + Misc.CERT_NAME);
        return request;
    }
}